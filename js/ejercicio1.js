const numBoletoInput = document.querySelector('#numBoletoInput');
const nomCliente = document.querySelector('#nomCliente');
const destinoInput = document.querySelector('#destinoInput');
const selectType = document.querySelector('#selectType')
const subtotalInput = document.querySelector('#SubtotalInput');
const inpuestosInput = document.querySelector('#impuestosInput');
const totalInput = document.querySelector('#totalInput');

const enviadoCont = document.querySelector('.respuestaContainer')

const calcularButton = document.querySelector('#calcularButton');
const limpiarButton = document.querySelector('#limpiarButton');
const enviarButton = document.querySelector('#enviarButton');


calcularButton.addEventListener('click', (e) => {
   e.preventDefault();
   let boletoValue = 950;
   let impuesto = .16;
   boletoValue = boletoValue * parseInt(selectType.value);
   subtotalInput.value = `$ : ${boletoValue}.00`;
   impuesto = boletoValue * impuesto
   inpuestosInput.value = `$ ${impuesto}.00`
   totalInput.value = `${boletoValue + impuesto}`

})

limpiarButton.addEventListener('click', (e) => {
   e.preventDefault();
   numBoletoInput.value = '';
   nomCliente.value = '';
   destinoInput.value = '';
   subtotalInput.value = '';
   inpuestosInput.value = '';
   totalInput.value = '';
})

enviarButton.addEventListener('click', (e) => {
   e.preventDefault();
   let element = document.createElement('p')
   element.innerHTML = "enviado con exito"
   enviadoCont.appendChild(element)
})

